<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_to_nomenclature".
 *
 * @property int $id
 * @property int $order_id id заказа
 * @property int $nom_id id номенклатуры
 * @property int $count количество
 * @property Nomenclature $nom
 * @property Order $order
 */
class OrderToNomenclature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_to_nomenclature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'nom_id'], 'integer'],
            [['nom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomenclature::className(), 'targetAttribute' => ['nom_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'id заказа',
            'nom_id' => 'id номенклатуры',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNom()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'nom_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return OrderToNomenclatureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderToNomenclatureQuery(get_called_class());
    }
}
