<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id

 * @property string $date Дата Заказа
 * @property string $address Адрес доставки
 * @property string $customer_phone Телефон заказчика
 * @property string $comment Примечания
 * @property int $status_id id статуса
 *  @property int $user_id id менеджера
 * @property float $sum суммаа
 * * @property OrderToNomenclature[] $noms
 * @property string $driver водитель
 * @property float $delivery_cost стоимость доставки
 *
 */
class Order extends \yii\db\ActiveRecord
{
    public $noms;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id','user_id'], 'integer'],
            [['date','noms','sum','delivery_cost','driver'], 'safe'],
            [['comment','driver'], 'string'],
            [['address', 'customer_phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата Заказа',
            'address' => 'Адрес доставки',
            'customer_phone' => 'Телефон заказчика',
            'comment' => 'Примечания',
            'status_id' => 'Cтатус',
            'noms' => 'Номенклатура',
            'sum' => 'Общая стоимость',
            'delivery_cost' => 'Стоимость достваки',
            'driver' => 'ФИО водителя',
            'user_id' => 'Менеджер',
        ];
    }
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $nomsAll = OrderToNomenclature::find()->where(['order_id' => $this->id,])->all();
        foreach ($nomsAll as $item1) {
            $a = false;
            if ($this->noms == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->noms as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if ($this->noms != null){
            foreach ($this->noms as $item) {
                $nom = OrderToNomenclature::find()->where(['id' => $item['id']])->one();
                if (!$nom) {
                    (new OrderToNomenclature([
                        'order_id' => $this->id,
                        'nom_id' => $item['nom_id'],
                        'count' => $item['count'],
                    ]))->save(false);
                }else{
                    $nom->nom_id = $item['nom_id'];
                    $nom->count = $item['count'];
                    $nom->save(false);
                }
            }


//            $allNoms = OrderToNomenclature::find()->where(['order_id' => $this->id])->all();
//            foreach ($allNoms as $num) {
//                if (array_search($num, $this->noms) !== false) {
//                    continue;
//
//                } else {
//                    $num->delete();
//                }
//            }
//            foreach ($this->noms as $nomGroup) {
//
//                $nomGroup2 = OrderToNomenclature::find()->where(['order_id' => $this->id, 'nom_id' => $nomGroup])->one();
//                if (!$nomGroup2) {
//                    (new OrderToNomenclature([
//                        'nom_id' => $nomGroup,
//                        'order_id' => $this->id
//                    ]))->save(false);
//                }
//            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToNomenclatures()
    {
        return $this->hasMany(OrderToNomenclature::className(), ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
