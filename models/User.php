<?php

namespace app\models;

use app\components\MyUploadedFile;
use DateTime;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $user_type тип
 * @property string $login Логин
 * @property string $name ФИО
 * @property string $email email
 * @property string $phone телефон
 * @property string $type тип пользователя
 * @property string $birth_date дата рождения
 * @property int $city_id город
 * @property int $work_city_id город выполненя работ
 * @property int $position_id Должность

 * @property int $inn Роль
 * @property int $can_update может изменять
 * @property int $can_view может смотреть
 * @property string $avatar Аватар
 * @property string $password_hash Зашифрованный пароль
 * @property int $access Доступ
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $created_at
 * @property string $of_name
 * @property string $address
 *
 * @property UploadedFile $file
 *

 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface, \rmrevin\yii\module\Comments\interfaces\CommentatorInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    public $password;



    private $oldPasswordHash;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'name', 'login', 'type', 'is_deletable',
                'password', 'password_hash',
                'can_update', 'can_view',
                'phone'],
            self::SCENARIO_EDIT => [
                'name', 'login', 'is_deletable', 'password',
                'can_update', 'can_view',
                'password_hash',
                'phone'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'name'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['can_update', 'can_view'], 'integer'],
            ['login', 'email'],
            [['login'], 'unique'],
            [['login', 'password_hash', 'password', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }
    }

    public function getCommentatorAvatar()
    {
        return '/'.$this->getRealAvatar();
    }

    public function getCommentatorName()
    {
        return $this->name;
    }

    public function getCommentatorUrl()
    {
        return ['user/view', 'id' => $this->id]; // or false, if user does not have a public page
    }

    /**
     * @param string $attribute
     */
    public function validateListFile($attribute)
    {
        foreach($this->$attribute as $index => $row) {
            if ($row['name'] == null) {
                $key = $attribute . '[' . $index . '][name]';
                $this->addError($key, 'Обязательное для заполненния');
            }
            foreach ($_FILES['User']['name'][$attribute] as $indexFile => $file){
                if($file['file_new'] == null){
                    $key = $attribute . '[' . $index . '][file_new]';
                    $this->addError($key, 'Обязательное для заполненния');
                }
            }
        }
    }




    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->is_deletable === 0;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {


            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s');
            }
            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            try {
                Yii::$app->mailer->compose()
                    ->setTo($this->login)
                    ->setFrom('hh.notify@yandex.ru')
                    ->setSubject('Вы зарегистрированы')
                    ->setHtmlBody("Ваш логин: {$this->login}<br> Ваш пароль: {$this->password}<br><a href='".Url::toRoute(['site/login'], true)."'>Войти в систему</a>")
                    ->send();
            } catch (\Exception $e)
            {
                Yii::warning('Email sending failed');
            }

            return true;
        }
        return false;
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Email',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Номер телефона ',
            'password_hash' => 'Password Hash',
            'is_deletable' => 'Можно ли удалять',
            'created_at' => 'Дата создания',
            'can_update' => 'Редактирование',
            'can_view' => 'Просмотр',

        ];
    }






    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getPosition()
//    {
//        return $this->hasOne(Position::className(), ['id' => 'position_id']);
//    }




    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }



}
