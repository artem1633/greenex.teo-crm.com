<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Nomenclature]].
 *
 * @see Nomenclature
 */
class NomenclatureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Nomenclature[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Nomenclature|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
