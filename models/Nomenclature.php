<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nomenclature".
 *
 * @property int $id
 * @property string $name Название
 * @property int $price Стоимость
 * @property int $count количество
 *
 * @property OrderToNomenclature[] $orderToNomenclatures
 */
class Nomenclature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nomenclature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'price' => 'Стоимость',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToNomenclatures()
    {
        return $this->hasMany(OrderToNomenclature::className(), ['nom_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return NomenclatureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NomenclatureQuery(get_called_class());
    }
}
