<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_to_nomenclature`.
 */
class m200522_014304_create_order_to_nomenclature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_to_nomenclature', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('id заказа'),
            'nom_id' => $this->integer()->comment('id номенклатуры')
        ]);

        $this->createIndex(
            'idx-order_to_nomenclature-order_id',
            'order_to_nomenclature',
            'order_id'
        );
        $this->createIndex(
            'idx-order_to_nomenclature-nom_id',
            'order_to_nomenclature',
            'nom_id'
        );
        $this->addForeignKey(
            'fk-order_to_nomenclature-order_id',
            'order_to_nomenclature',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-order_to_nomenclature-nom_id',
            'order_to_nomenclature',
            'nom_id',
            'nomenclature',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order_to_nomenclature-nom_id',
            'order_to_nomenclature'
        );

        $this->dropForeignKey(
            'fk-order_to_nomenclature-order_id',
            'order_to_nomenclature'
        );
        $this->dropIndex(
            'idx-order_to_nomenclature-nom_id',
            'order_to_nomenclature'
        );
        $this->dropIndex(
            'idx-order_to_nomenclature-order_id',
            'order_to_nomenclature'
        );
        $this->dropTable('order_to_nomenclature');
    }
}
