<?php

use yii\db\Migration;

/**
 * Class m200527_021759_change
 */
class m200527_021759_change extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_to_nomenclature', 'count', $this->integer()->comment('количество'));
        $this->addColumn('order', 'sum', $this->float()->comment('Общая стоимость'));
        $this->addColumn('order', 'delivery_cost', $this->float()->comment('Стоимость доостваки'));
        $this->addColumn('order', 'driver', $this->string()->comment('ФИО водителя'));
        $this->addColumn('order', 'user_id', $this->integer()->comment('id пользователя'));
        $this->addColumn('user', 'can_update', $this->boolean()->comment('Право изменения'));
        $this->addColumn('user', 'can_view', $this->boolean()->comment('Право удаления'));

        $this->dropColumn('order', 'number');

        $this->createIndex(
            'idx-order-user_id',
            'order',
            'user_id'
        );
        $this->addForeignKey(
            'fk-order-user_id',
            'order',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-user_id',
            'order'
        );
        $this->dropIndex(
            'idx-order-user_id',
            'order'
        );
        $this->addColumn('order', 'number', $this->integer()->comment('номер заказа'));
        $this->dropColumn('order', 'user_id');
        $this->dropColumn('order', 'sum');
        $this->dropColumn('order', 'delivery_cost');
        $this->dropColumn('order', 'driver');
        $this->dropColumn('order_to_nomenclature', 'count');
        $this->dropColumn('user', 'can_update');
        $this->dropColumn('user', 'can_view');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200527_021759_change cannot be reverted.\n";

        return false;
    }
    */
}
