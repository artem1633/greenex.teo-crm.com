<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nomenclature`.
 */
class m200522_012050_create_nomenclature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nomenclature', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'price' => $this->integer()->comment('Стоимость')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nomenclature');
    }
}
