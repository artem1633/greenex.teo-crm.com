<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m200522_014211_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->comment('номер заказа'),
            'date' => $this->date()->comment('Дата Заказа'),
            'address' => $this->string()->comment('Адрес доставки'),
            'customer_phone' => $this->string()->comment('Телефон заказчика'),
            'comment' => $this->text()->comment('Примечания'),
            'status_id' => $this->integer()->comment('id статуса')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');
    }
}



