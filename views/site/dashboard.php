<?php
use yii\helpers\Url;
use app\models\User;
use yii\helpers\Html;

$this->title = 'Рабочий стол';
?>



    <div class="row">

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                <div class="stats-info">
                    <p>ПРОЕКТЫ</p>

                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/contract'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-chain-broken"></i></div>
                <div class="stats-info">
                    <p>ЗАДАЧИ</p>

                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/task'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-purple">
                <div class="stats-icon"><i class="fa fa-users"></i></div>
                <div class="stats-info">
                    <p>РЕЙТИНГ</p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-red">
                <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
                <div class="stats-info">
                    <p>СУММА ЧАСОВ</p>

                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/task'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-8">
            <div class="panel panel-inverse" data-sortable-id="index-5">
                <div class="panel-heading">
                    <div class="panel-heading-btn">

                    </div>
                    <h4 class="panel-title">Комментарии</h4>
                </div>
                <div class="panel-body page-container-width1 desc-page-container">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                        <div  data-scrollbar="true" data-init="true" style="overflow: hidden; width: auto; height: auto;">
                            <ul class="media-list media-list-with-divider media-messaging">
                                <?php
//                                var_dump($comment);
//                                die();
                                foreach ($comment as $com) {

                                    $user = User::findOne($com['id']);
                                    if($user->avatar == '' || $user->avatar == null) $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/avatar.jpg';
                                    else $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$user->id.'.jpg';
                                    ?>

                                    <li class="media media-sm">
                                        <a href="javascript:;" class="pull-left">
                                            <!--  <img src="http://seantheme.com/color-admin-v3.0/admin/html/assets/img/user-5.jpg" alt="" class="media-object rounded-corner"> -->
                                            <img src="<?=$path?>" alt="" class="media-object rounded-corner">
                                            <?php /*Html::img($path, [
                                        'style' => 'width:80px; height:80px;',
                                        'class' => 'img-circle',
                                    ])*/ ?>
                                        </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"><?=$com['user']?> &nbsp &nbsp &nbsp Проект : <?=$com['title']?></h5>
                                            <p><?=$com['text']?></p>
                                        </div>
                                    </li>

                                <?php } ?>


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin col-8 -->
        <div class="col-md-4 ui-sortable">
            <div class="panel panel-inverse" data-sortable-id="index-8">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Общий список задач</h4>
                </div>
                <div class="panel-body p-0 page-container-width1 desc-page-container">
                    <ul class="todolist">


                        <?php foreach ($tasks as $task) { $url = Url::to(['task/view', 'id' => $task->id]); ?>

                            <li>
                                <a href="<?=$url?>" class="todolist-container" data-click="todolist">
                                    <div class="todolist-title"><?= $task->name?></div>
                                </a>
                            </li>

                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php

$this->registerJsFile('/theme/assets/js/dashboard.min.js', ['position' => yii\web\View::POS_READY]);

?>