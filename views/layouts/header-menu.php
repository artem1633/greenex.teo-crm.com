<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [

                    ['label' => 'Заказы', 'icon' => 'fa  fa-flag', 'url' => ['/order'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Номенклатура', 'icon' => 'fa fa-sitemap', 'url' => ['/nomenclature'],],
                    ['label' => 'Статус', 'icon' => 'fa fa-area-chart', 'url' => ['/status'],],

            ]
    ]
        );
        ?>
    <?php endif; ?>
</div>
