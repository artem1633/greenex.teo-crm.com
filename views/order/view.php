<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
?>
<div class="order-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'address',
            'customer_phone',
            'comment',
            'status_id',
            'sum',
            'delivery_cost',
            'driver',
            'user_id',
        ],
    ]) ?>

</div>
