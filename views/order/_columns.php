<?php

use app\models\Nomenclature;
use app\models\OrderToNomenclature;
use app\models\Status;
use app\models\User;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
         [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'id',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
//        'filterInputOptions' => ['class' => 'form-control', 'type' => 'date'],
        'filterInputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginEvents' => [
                'cancel.daterangepicker'=>'function(ev, picker) {$("#contractsearch-contract_date").val(""); $("#contractsearch-contract_date").trigger("change"); }'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'convertFormat'=>true,
                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'YYYY-MM-DD'
                ]
            ],
        ],
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'noms',
        'label' => 'Номенклатура',
        'content' => function($model){
            $info = '';
            foreach (OrderToNomenclature::find()->where(['order_id' => $model->id])->all() as $item){
                $n = Nomenclature::findOne($item->nom_id);
                $info .= $n->name." ({$item->count}), ";
            }
            return $info;
        },
        'filter' => ArrayHelper::map(Nomenclature::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'delivery_cost',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'driver',
    ],

    [
        'attribute' => 'status_id',
        'value' => function ($model, $key, $index, $widget) {
            $statusName = implode('',ArrayHelper::getColumn(Status::find()->where(['id' => $model->status_id])->all(), 'name'));
            $statusColor = implode('', ArrayHelper::getColumn(Status::find()->where(['id' => $model->status_id])->all(), 'color'));
            return "<div style='display: flex;background-color: {$statusColor};color: black'> <span>{$statusName}</span></div>";

        },
        'width' => '',
        'filter' => ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'name'),
        'filterType' => 'dropdown',
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(User::find()->where(['id' => $data->user_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'visibleButtons' => [
            'update' => function ($model, $key, $index) {
                return Yii::$app->user->identity->can_update === 1 or Yii::$app->user->identity->isSuperAdmin();
            },
            'delete' => function ($model, $key, $index) {
                return Yii::$app->user->identity->isSuperAdmin();
            },
            'view' => function ($model, $key, $index){

                return Yii::$app->user->identity->can_view === 1 or Yii::$app->user->identity->isSuperAdmin();
            }

        ],
//        Yii::$app->user->identity->can_update === 1
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];