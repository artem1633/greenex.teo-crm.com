<?php

use app\models\Nomenclature;
use app\models\OrderToNomenclature;
use app\models\Status;
use app\models\User;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'noms')->widget(MultipleInput::className(), [

        'id' => 'my_id',
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden'
                ],
            ],
            [
                    'name'  => 'nom_id',
                    'type'  => 'dropDownList',
                    'title' => 'Номенклатура',
                    'items' => ArrayHelper::map(Nomenclature::find()->orderBy('name asc')->all(), 'id', 'name')
            ],
            [
                'name' => 'count',
                'title' => 'Колчество',
            ],


        ],
    ]) ?>

   <div class="row">
       <div class="col-md-6">
           <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>
       </div>
       <div class="col-md-6">
           <?= $form->field($model, 'date')->input('date') ?>
       </div>
   </div>


  <div class="row">
      <div class="col-md-6">
          <?= $form->field($model, 'delivery_cost')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-6">
          <?= $form->field($model, 'customer_phone')->widget(\yii\widgets\MaskedInput::class, [
              'mask' => '+7 999 999-99-99',
          ]) ?>
      </div>
  </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'driver')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
   <div class="row">
       <div class="col-md-6">
           <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->orderBy('name asc')->all(), 'id', 'name'))?>

       </div>
       <div class="col-md-6">
           <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(Status::find()->orderBy('name asc')->all(), 'id', 'name'))?>
       </div>
   </div>
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>



  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
