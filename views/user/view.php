<?php

use app\models\City;
use app\models\Subgroups;
use app\models\UserKind;

use app\models\UserType;
use app\models\WorkKind;
use app\models\WorkType;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

?>
<div class="user-view">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
            <div class="panel-heading-btn" style="margin-top: -20px;">
                <?= Html::a('<span class="glyphicon glyphicon-pencil "></span>', ['user/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-user-info-container'], ['role' => 'modal-remote']) ?>
            </div>
        </div>
        <div class="panel-body">
 
    <?php Pjax::begin(['id' => 'pjax-user-info-container', 'enablePushState' => false]); ?>
            <div class="row">

                <div class="col-md-12">
                    <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                            'phone',
                            'name',
                            'login:email',
                    ],
                    ]) ?>
                </div>






            </div>



        </div>
    <?php Pjax::end(); ?>



    </div>



</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>