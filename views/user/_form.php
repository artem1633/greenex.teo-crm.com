<?php


use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="user-form">
    <div class="modal-teo">

        <div class="row">

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'login')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                            'mask' => '+7 999 999-99-99',
                        ]) ?>
                    </div>
                </div>
                <?= $form->field($model, 'can_update')->checkbox([
                    'uncheck'=>0,
                    'checked'=>1,
                ],true) ?>
                <?= $form->field($model, 'can_view')->checkbox([
                    'uncheck'=>0,
                    'checked'=>1,
                ],true) ?>

            </div>
        </div>








        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

    </div>


    <?php ActiveForm::end(); ?>

